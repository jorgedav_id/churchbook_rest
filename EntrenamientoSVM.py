import numpy as np
import cv2
import os
from sklearn.svm import LinearSVC
from sklearn.externals import joblib
from sklearn.preprocessing import StandardScaler
from scipy.cluster.vq import *

listaRutaImagenesEntrenamiento = []
listaClaseImagenes = []
idClase = 0
listaDescriptores = []

def listarImagenesEntrenamiento (path):
	return [os.path.join(path, f) for f in os.listdir(path)]

directorioEntrenamiento = "E:\\Opencv_Tesis\\Entrenamiento"
nombreClasesObjetos = os.listdir(directorioEntrenamiento)

for clase in nombreClasesObjetos:
	directorioClase = os.path.join(directorioEntrenamiento, clase)
	listaRutaImagenesClase = listarImagenesEntrenamiento(directorioClase)
	listaRutaImagenesEntrenamiento += listaRutaImagenesClase
	listaClaseImagenes += [idClase] * len(listaRutaImagenesClase)
	idClase += 1

sift = cv2.xfeatures2d.SIFT_create()

for rutaImagen in listaRutaImagenesEntrenamiento:
	imagen = cv2.imread(rutaImagen)
	kpts = sift.detect(imagen)
	kpts, descriptoresImagen = sift.compute(imagen, kpts)
	listaDescriptores.append((rutaImagen, descriptoresImagen))

descriptoresEntrenamiento = listaDescriptores[0][1]

for rutaImagen, descriptor in listaDescriptores[1:]:
	descriptor = np.vstack((descriptoresEntrenamiento, descriptor))

k = 100
voc, varianza = kmeans(descriptoresEntrenamiento, k, 1)

caracteristicasImagen = np.zeros((len(listaRutaImagenesEntrenamiento), k), "float32")

for i in range(len(listaRutaImagenesEntrenamiento)):
	words, distancia = vq(listaDescriptores[i][1], voc)
	for w in words:
		caracteristicasImagen [i][w] += 1

nbr_occurences = np.sum((caracteristicasImagen > 0) * 1, axis = 0)
idf = np.array(np.log((1.0 * len(listaRutaImagenesEntrenamiento) + 1) / (1.0 * nbr_occurences + 1)), 'float32')

scalarStandar = StandardScaler().fit(caracteristicasImagen)
caracteristicasImagen = scalarStandar.transform(caracteristicasImagen)

clasificadorSVM = LinearSVC()
clasificadorSVM.fit(caracteristicasImagen, np.array(listaClaseImagenes))

joblib.dump((clasificadorSVM, nombreClasesObjetos, scalarStandar, k, voc), "bof.pkl", compress = 3)