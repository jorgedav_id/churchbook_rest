import pymysql.cursors
import json

conexion = pymysql.connect(host='localhost', 
	user="root", 
	password="mysql",
	db="CHURCHBOOK",
	cursorclass=pymysql.cursors.DictCursor)

def consultaSql(nombreClaseIglesia):
	try:
		with conexion.cursor() as cursor:
			sqlConsulta = "SELECT cIdImagenDescripcion, cTitulo, cDescripcion, dFechaConstruccion FROM Imagen_Descripcion WHERE cIdImagenDescripcion=%s"
			cursor.execute(sqlConsulta, (nombreClaseIglesia,))
			result = cursor.fetchone()
			return json.dumps(result)
			sqljson = json.dump(result)
			conexion.close()
	finally:
		connection.close()

def consultaImagenes(nombreClaseIglesia):
	try:
		with conexion.cursor() as cursor:
			sqlConsulta = "SELECT cURL FROM Galeria WHERE cIdGaleria=%s"
			cursor.execute(sqlConsulta, (nombreClaseIglesia))
			result = cursor.fetchall()
			return json.dumps(result)
	finally:
		conexion.close
