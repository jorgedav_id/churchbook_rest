# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 23:21:56 2016

@author: Jorge David
"""
import cv2
import numpy as np
import os
from scipy.cluster.vq import *
from sklearn.svm import LinearSVC
from sklearn.externals import joblib

def leerImagen(ruta):
    archivoImagen = cv2.imread(ruta)
    if(archivoImagen == None):
        print("Error al leer el archivo ", rutaImagen)
        exit()
    return archivoImagen

def clasificarImagen(imagen):
    clasificador, nombreClases, standardScaler, klusters, vocabulario = joblib.load("bof.pkl")
    sift = cv2.xfeatures2d.SIFT_create()
    imagenaClasificar = imagen
    listaDescriptores = []
    prediccion = []
    kpts = sift.detect(imagenaClasificar)
    kpts, descriptoresImagen = sift.compute(imagenaClasificar, kpts)
    listaDescriptores.append((imagenaClasificar, descriptoresImagen))
    descriptores = listaDescriptores[0][1]
    descriptores = np.vstack((descriptores, listaDescriptores[0][1]))
    caracteristicas = np.zeros((1, klusters), "float32")
    words, distancia = vq(listaDescriptores[0][1], vocabulario)
    for w in words:
        caracteristicas[0][w] += 1
    nbr_occurences = np.sum( (caracteristicas > 0) * 1, axis = 0)
    idf = np.array(np.log((1.0 * 1 + 1) / (1.0 * nbr_occurences + 1)), "float32")
    caracteristicas = standardScaler.transform(caracteristicas)
    prediccion = [nombreClases[i] for i in clasificador.predict(caracteristicas)]
    return prediccion[0]
