from flask import Flask, request
from flask_restful import Api, Resource
import Clasificacion as clasificador
import conexionBaseDatos
from werkzeug import secure_filename
from flask_restful import reqparse
import cv2
import os
import json
import base64

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('file', type=str)
parser.add_argument('iglesia', type=str)

@api.representation('application/json')
def output_json(data, code, headers=None):
    resp = make_response(json.dumps(data), code)
    resp.headers.extend(headers or {})
    return resp

@app.route('/apiOpencv', methods=['POST'])
def apiOpencv():
	args = parser.parse_args()
	file = str(args['file'])
	imgdata = base64.b64decode(file)
	with open("imagenaClasificar.jpg", "wb") as f:
		f.write(imgdata)
		f.close()
	rutaFoto = "E:\\Opencv_Tesis\\imagenaClasificar.jpg"
	app.logger.debug('archivo recibido: %s', rutaFoto)
	fotoaClasificar = clasificador.leerImagen(rutaFoto)
	prediccion = clasificador.clasificarImagen(fotoaClasificar)
	datosClasificacion = conexionBaseDatos.consultaSql(prediccion)
	app.logger.debug('Respuesta: %s', json.loads(json.dumps(datosClasificacion)))
	return json.loads(json.dumps(datosClasificacion)), 200

@app.route('/galeria', methods=['POST'])
def galeria():
	args = parser.parse_args()
	nombreIglesia = str(args['iglesia'])
	imagenes = conexionBaseDatos.consultaImagenes(nombreIglesia)
	app.logger.debug('resultado: %s', json.loads(json.dumps(imagenes)))
	return json.loads(json.dumps(imagenes)), 200

if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True)